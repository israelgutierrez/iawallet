//
//  ViewController.swift
//  IAWallet
//
//  Created by Israel Gutiérrez Castillo on 12/13/2017.
//  Copyright (c) 2017 Israel Gutiérrez Castillo. All rights reserved.
//

import UIKit
import IAWallet

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let viewWallet = WalletView.init(frame: CGRect.init(x: 0.0,
                                                            y: 0.0,
                                                        width: 100.0,
                                                       height: 100.0))
        self.view.addSubview(viewWallet)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

