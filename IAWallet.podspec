#
# Be sure to run `pod lib lint IAWallet.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'IAWallet'
  s.version          = '0.1.0'
  s.summary          = 'View that looks like native wallet.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
View that looks like native wallet. You can drag and drop CardView and WalletView.
                       DESC

  s.homepage         = 'https://israelgutierrez@bitbucket.org/israelgutierrez/iawallet.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Israel Gutiérrez Castillo' => 'igutierrez@ia.com.mx' }
  s.source           = { :git => 'https://israelgutierrez@bitbucket.org/israelgutierrez/iawallet.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'IAWallet/Classes/**/*'
  
  # s.resource_bundles = {
  #   'IAWallet' => ['IAWallet/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
