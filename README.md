# IAWallet

[![CI Status](http://img.shields.io/travis/Israel Gutiérrez Castillo/IAWallet.svg?style=flat)](https://travis-ci.org/Israel Gutiérrez Castillo/IAWallet)
[![Version](https://img.shields.io/cocoapods/v/IAWallet.svg?style=flat)](http://cocoapods.org/pods/IAWallet)
[![License](https://img.shields.io/cocoapods/l/IAWallet.svg?style=flat)](http://cocoapods.org/pods/IAWallet)
[![Platform](https://img.shields.io/cocoapods/p/IAWallet.svg?style=flat)](http://cocoapods.org/pods/IAWallet)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IAWallet is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IAWallet'
```

## Author

Israel Gutiérrez Castillo, igutierrez@ia.com.mx

## License

IAWallet is available under the MIT license. See the LICENSE file for more info.
